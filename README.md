# test

## Установка зависимостей
npm install

## Режим разработчика
npm run dev

Проект будет доступен по адресу:
http://localhost:8081/

## Режим тестирования
npm run test

## Режим сборки
npm run build
