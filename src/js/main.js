'use strict'

import Vue from 'vue'

// hides dev-mode warning in console
Vue.config.productionTip = false

// vue init
let app = new Vue({
  el: '#app',
  data: {
    posts: [],
    users: [],
    search: ''
  },
  created() {
    this.getUsers(),
    this.getPosts()
  },
  methods: {
    getPosts() {
      fetch('https://jsonplaceholder.typicode.com/posts')
      .then(response => response.json())
      .then(result => this.posts = result)
    },
    getUserPosts(userId) {
      fetch('https://jsonplaceholder.typicode.com/posts?userId=' + userId)
      .then(response => response.json())
      .then(result => this.posts = result)
    },
    getUsers() {
      fetch('https://jsonplaceholder.typicode.com/users')
      .then(response => response.json())
      .then(result => this.users = result)
    },
    user(userId) {
      return this.users.find(user => user.id == userId)
    },
    searchAuthor() {
      const search = this.search.toLowerCase()

      if (search) {
        this.users.filter(user => {
          return !!~user.name.toLowerCase().indexOf(search) ? this.getUserPosts(user.id) : this.posts = []
        })
      } else {
        return this.getPosts()
      }
    }
  }
})
